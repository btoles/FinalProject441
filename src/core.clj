(ns merge_sort
  (:use [clojure.java.io :only (reader)])
  (:gen-class :main true))

(defn merj [left right]
  (loop [[lhead & ltail :as left] left
         [rhead & rtail :as right] right
         result []]
    (cond (nil? left) (concat result right)
          (nil? right) (concat result left)
          true (if (<= lhead rhead)
                 (recur ltail right (conj result lhead))
                 (recur left rtail (conj result rhead))))))

(defn sawt [lst]
  (let [array lst
        middle (bit-shift-right (count array) 1)
        [left right] (split-at middle array)]
    (if (= 1 (count array))
      array
      (merj (sawt left) (sawt right)))))

(defn -main [fname]
  (with-open [rdr (reader fname)]
    (println (sawt (map #(Integer/parseInt %) (line-seq rdr))))))

(-main "numbers_txt.txt")