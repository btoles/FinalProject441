(ns main)
(use 'clojure.java.io)

(defn read-file [fname]
  (with-open [r (reader fname)]
    (doall (map #(String/valueOf %) (line-seq r)))))

(defn combine [left, right]
  (loop [l left, r right, lhead (first left), rhead (first right), result []]
    (cond
      (nil? lhead)  (conj result r)
      (nil? rhead)  (conj result l)
      true (if (<= lhead rhead)
             (recur (rest l) r (first (rest l)) (first r) (conj result lhead))
             (recur l (rest r) (first l) (first (rest r)) (conj result rhead))))))

(defn merge-sort [data]
  (let [lst data,
        mid (int (Math/floor (/ (count data) 2))),
        [left right] (split-at mid lst)]
    (if (<= 1 (count data))
      data
      (combine (merge-sort left) (merge-sort right)))))


(doseq [item (merge-sort (read-file "numbers_txt.txt"))]
  (println item))